package org.example.service.impl;

import org.example.dao.TraineeRepository;
import org.example.dao.UserRepository;
import org.example.dto.TraineeCreateDto;
import org.example.dto.TraineeResponseDto;
import org.example.dto.TraineeUpdateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.UserEntity;
import org.example.service.UserCredentialsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

class TraineeServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private UserCredentialsService userCredentialsService;

    @InjectMocks
    private TraineeServiceImpl traineeService;
    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldCreateTrainee() {
        //given
        TraineeCreateDto traineeCreateDto = new TraineeCreateDto("a", "a", "12/12/2003", "fergana");

        //mocks
        when(userCredentialsService.generatePassword()).thenReturn("1234567890");
        when(userCredentialsService.getUsername("a","a")).thenReturn("a.a");
        when(userRepository.save(any())).thenReturn(new UserEntity(1L));
        when(traineeRepository.save(any())).thenReturn(new TraineeEntity(1L));

        //when
        Long traineeId = traineeService.createTrainee(traineeCreateDto);

        //then
        assertThat(traineeId).isEqualTo(1L);
    }

    @Test
    void shouldUpdateTrainee(){
        //given
        TraineeUpdateDto traineeUpdateDto = new TraineeUpdateDto("12/12/2003", "fergana");
        Long traineeId = 1L;

        //mocks
        when(traineeRepository.findById(traineeId)).thenReturn(Optional.of(new TraineeEntity(traineeId)));
        when(traineeRepository.save(any())).thenReturn(new TraineeEntity(traineeId));

        //when
        TraineeEntity trainee = traineeService.updateTrainee(traineeUpdateDto, traineeId);

        //then
        assertThat(trainee.getId()).isEqualTo(traineeId);
    }

    @Test
    void shouldThrowExceptionWhenUpdateTraineeIfTraineeNotFound(){
        //given
        TraineeUpdateDto traineeUpdateDto = new TraineeUpdateDto("12/12/2003", "fergana");
        Long traineeId = 1L;

        //mocks
        when(traineeRepository.findById(traineeId)).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> traineeService.updateTrainee(traineeUpdateDto, traineeId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainee not found!");
    }

    @Test
    void shouldDeleteTrainee(){
        //given
        Long traineeId = 1L;

        //when
        traineeService.deleteTrainee(traineeId);

        //then
        verify(traineeRepository).delete(traineeId);
    }

    @Test
    void shouldGetTrainee(){
        //given
        Long traineeId = 1L;

        //mocks
        when(traineeRepository.findById(traineeId)).thenReturn(Optional.of(new TraineeEntity(traineeId)));
        when(userRepository.findById(any())).thenReturn(Optional.empty());

        //when
        TraineeResponseDto trainee = traineeService.getTrainee(traineeId);

        //then
        assertThat(trainee.getId()).isEqualTo(traineeId);
    }

    @Test
    void shouldThrowExceptionWhenGetTraineeIfTraineeNotFound(){
        //given
        Long traineeId = 1L;

        //mocks
        when(traineeRepository.findById(traineeId)).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> traineeService.getTrainee(traineeId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainee not found!");
    }
}