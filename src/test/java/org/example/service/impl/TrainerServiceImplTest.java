package org.example.service.impl;

import org.example.dao.TrainerRepository;
import org.example.dao.UserRepository;
import org.example.dto.TrainerCreateDto;
import org.example.dto.TrainerResponseDto;
import org.example.dto.TrainerUpdateDto;
import org.example.entity.TrainerEntity;
import org.example.entity.UserEntity;
import org.example.service.UserCredentialsService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class TrainerServiceImplTest {
    @Mock
    private UserRepository userRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private UserCredentialsService userCredentialsService;

    @InjectMocks
    private TrainerServiceImpl trainerService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldCreateTrainer() {
        //given
        TrainerCreateDto trainerCreateDto = new TrainerCreateDto(1L, "a", "a");

        //mocks
        when(userRepository.save(any())).thenReturn(new UserEntity(1L));
        when(userCredentialsService.getUsername("a","a")).thenReturn("a.a");
        when(userCredentialsService.generatePassword()).thenReturn("1234567890");
        when(trainerRepository.save(any())).thenReturn(new TrainerEntity(1L));

        //when
        Long trainerId = trainerService.createTrainer(trainerCreateDto);

        //then
        assertThat(trainerId).isEqualTo(1L);
    }

    @Test
    void shouldUpdateTrainer(){
        //given
        TrainerUpdateDto trainerUpdateDto = new TrainerUpdateDto(1L);
        Long trainerId = 1L;
        TrainerEntity trainer = new TrainerEntity(1L, 2L, 1L);
        TrainerEntity updatedTrainer = new TrainerEntity(1L, 1L, 1L);

        //mocks
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.of(trainer));
        when(trainerRepository.save(any())).thenReturn(updatedTrainer);

        //when
        TrainerEntity response = trainerService.updateTrainer(trainerUpdateDto, trainerId);

        //then
        assertThat(response.getSpecializationId()).isEqualTo(trainerUpdateDto.getSpecializationId());
    }

    @Test
    void shouldThrowExceptionWhenUpdatingTrainerIfTrainerNotFound(){
        //given
        TrainerUpdateDto trainerUpdateDto = new TrainerUpdateDto(1L);
        Long trainerId = 1L;

        //mocks
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> trainerService.updateTrainer(trainerUpdateDto, trainerId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainer is not found!");
    }

    @Test
    void shouldGetTrainer(){
        //given
        Long trainerId = 1L;
        TrainerEntity trainer = new TrainerEntity(1L, 2L, 1L);


        //mocks
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.of(trainer));
        when(userRepository.findById(trainer.getUserId())).thenReturn(Optional.of(new UserEntity(trainer.getUserId())));

        //when
        TrainerResponseDto trainerResponseDto = trainerService.getTrainer(trainerId);

        //then
        assertThat(trainerResponseDto.getId()).isEqualTo(trainerId);
        assertThat(trainerResponseDto.getUserId()).isEqualTo(trainer.getUserId());
    }

    @Test
    void shouldThrowExceptionWhenGettingTrainerIfTrainerNotFound(){
        //given
        Long trainerId = 1L;

        //mocks
        when(trainerRepository.findById(trainerId)).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> trainerService.getTrainer(trainerId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainer is not found!");

    }
}