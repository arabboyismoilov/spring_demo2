package org.example.service.impl;

import org.example.dao.TraineeRepository;
import org.example.dao.TrainerRepository;
import org.example.dao.TrainingRepository;
import org.example.dao.TrainingTypeRepository;
import org.example.dto.TrainingCreateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.TrainerEntity;
import org.example.entity.TrainingEntity;
import org.example.entity.TrainingType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;


class TrainingServiceImplTest {

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TraineeRepository traineeRepository;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @InjectMocks
    private TrainingServiceImpl trainingService;

    @BeforeEach
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void shouldCreateTraining() {
        //given
        TrainingCreateDto trainingCreateDto = new TrainingCreateDto(
                1L, 2L,
                "Tennis", 1L,
                "12/12/2024", 30
        );

        //mocks
        Mockito.when(traineeRepository.findById(any())).thenReturn(Optional.of(new TraineeEntity(1L)));
        Mockito.when(trainerRepository.findById(any())).thenReturn(Optional.of(new TrainerEntity(2L)));
        Mockito.when(trainingTypeRepository.findById(any())).thenReturn(Optional.of(new TrainingType(1L, "tennis")));
        Mockito.when(trainingRepository.save(any())).thenReturn(new TrainingEntity(
                1L,
                1L,
                2L,
                trainingCreateDto.getTrainingName(),
                1L,
                trainingCreateDto.getTrainingDate(),
                trainingCreateDto.getDuration()
        ));

        //when
        Long trainingId = trainingService.createTraining(trainingCreateDto);

        //then
        assertThat(trainingId).isEqualTo(1L);

        //verify
        verify(traineeRepository).findById(any());
        verify(trainerRepository).findById(any());
        verify(trainingTypeRepository).findById(any());
        verify(trainingRepository).save(any());
    }


    @Test
    void shouldThrowExceptionWhenCreateTrainingIfTraineeNotFound() {
        //given
        TrainingCreateDto trainingCreateDto = new TrainingCreateDto(
                1L, 2L,
                "Tennis", 1L,
                "12/12/2024", 30
        );

        //mocks
        Mockito.when(traineeRepository.findById(any())).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> trainingService.createTraining(trainingCreateDto))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainee not found!");

        //verify
        verify(traineeRepository).findById(any());
        verify(trainerRepository, never()).findById(any());
        verify(trainingTypeRepository, never()).findById(any());
        verify(trainingRepository, never()).save(any());
    }

    @Test
    void shouldThrowExceptionWhenCreateTrainingIfTrainerNotFound() {
        //given
        TrainingCreateDto trainingCreateDto = new TrainingCreateDto(
                1L, 2L,
                "Tennis", 1L,
                "12/12/2024", 30
        );

        //mocks
        Mockito.when(traineeRepository.findById(any())).thenReturn(Optional.of(new TraineeEntity(1L)));
        Mockito.when(trainerRepository.findById(any())).thenReturn(Optional.empty());


        //when
        //then
        assertThatThrownBy(() -> trainingService.createTraining(trainingCreateDto))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Trainer not found!");

        //verify
        verify(traineeRepository).findById(any());
        verify(trainerRepository).findById(any());
        verify(trainingTypeRepository, never()).findById(any());
        verify(trainingRepository, never()).save(any());
    }

    @Test
    void shouldThrowExceptionWhenCreateTrainingIfTrainingTypeNotFound() {
        //given
        TrainingCreateDto trainingCreateDto = new TrainingCreateDto(
                1L, 2L,
                "Tennis", 1L,
                "12/12/2024", 30
        );

        //mocks
        Mockito.when(traineeRepository.findById(any())).thenReturn(Optional.of(new TraineeEntity(1L)));
        Mockito.when(trainerRepository.findById(any())).thenReturn(Optional.of(new TrainerEntity(2L)));
        Mockito.when(trainingTypeRepository.findById(any())).thenReturn(Optional.empty());


        //when
        //then
        assertThatThrownBy(() -> trainingService.createTraining(trainingCreateDto))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Training type not found!");

        //verify
        verify(traineeRepository).findById(any());
        verify(trainerRepository).findById(any());
        verify(trainingTypeRepository).findById(any());
        verify(trainingRepository, never()).save(any());
    }


    @Test
    void shouldGetTraining() {
        //given
        Long trainingId = 1L;

        //mocks
        Mockito.when(trainingRepository.findById(any())).thenReturn(Optional.of(new TrainingEntity(trainingId)));

        //when
        TrainingEntity training = trainingService.getTraining(trainingId);

        //then
        assertThat(training.getId()).isEqualTo(trainingId);

        //verify
        verify(trainingRepository).findById(any());
    }

    @Test
    void shouldThrowExceptionIfTrainingIsNotExistByGivenId(){
        //given
        Long trainingId = 1L;

        //mocks
        Mockito.when(trainingRepository.findById(any())).thenReturn(Optional.empty());

        //when
        //then
        assertThatThrownBy(() -> trainingService.getTraining(trainingId))
                .isInstanceOf(RuntimeException.class)
                .hasMessage("Training not found!");

        //verify
        verify(trainingRepository).findById(any());
    }
}