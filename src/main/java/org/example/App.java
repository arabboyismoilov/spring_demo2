package org.example;

import org.example.config.SpringConfiguration;
import org.example.dto.TrainerCreateDto;
import org.example.service.TrainerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


/**
 * Hello world!
 *
 */
public class App
{
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    public static void main( String[] args )
    {
        logger.info("Spring application started");
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(SpringConfiguration.class);

        //reading initial trainer data
        TrainerService trainerService = applicationContext.getBean(TrainerService.class);
        System.out.println(trainerService.getTrainer(1L));

        //creating new trainer
        Long trainerId = trainerService.createTrainer(new TrainerCreateDto(1L, "Hanks", "Tom"));

        //reading created trainer
        System.out.println(trainerService.getTrainer(trainerId));
    }
}
