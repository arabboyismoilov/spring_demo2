package org.example.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.dao.database.Storage;
import org.example.service.ReadFileService;
import org.example.service.impl.ReadFileServiceImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(basePackages = "org.example")
@PropertySource("classpath:application.properties")
public class SpringConfiguration {
    @Value("${initial.data}")
    private String initialData;

    @Bean
    public ObjectMapper objectMapper(){
        return new ObjectMapper();
    }

    @Bean
    public Storage storage(){
        Storage storage = new Storage(readFileService(), initialData);
        storage.init();
        return storage;
    }

    @Bean
    public ReadFileService readFileService(){
        return new ReadFileServiceImpl(objectMapper());
    }
}
