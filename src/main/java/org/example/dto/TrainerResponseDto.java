package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrainerResponseDto {
    private Long id;
    private Long specializationId;
    private String lastName;
    private String firstName;
    private String username;
    private String password;
    private Long userId;
}
