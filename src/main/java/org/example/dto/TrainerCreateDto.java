package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TrainerCreateDto {
    private Long specializationId; //training type id
    private String lastName;
    private String firstName;
}
