package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TraineeResponseDto {
    private Long id;
    private String lastName;
    private String firstName;
    private String username;
    private String password;
    private String dateOfBirth;
    private String address;
    private Long userId;
}
