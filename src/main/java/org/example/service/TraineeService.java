package org.example.service;

import org.example.dto.TraineeCreateDto;
import org.example.dto.TraineeResponseDto;
import org.example.dto.TraineeUpdateDto;
import org.example.entity.TraineeEntity;

public interface TraineeService {
    Long createTrainee(TraineeCreateDto traineeCreateDto);
    TraineeEntity updateTrainee(TraineeUpdateDto traineeUpdateDto, Long id);
    void deleteTrainee(Long id);
    TraineeResponseDto getTrainee(Long id);
}
