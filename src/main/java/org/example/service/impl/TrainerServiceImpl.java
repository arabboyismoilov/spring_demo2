package org.example.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TrainerRepository;
import org.example.dao.UserRepository;
import org.example.dto.TrainerCreateDto;
import org.example.dto.TrainerResponseDto;
import org.example.dto.TrainerUpdateDto;
import org.example.entity.TrainerEntity;
import org.example.entity.UserEntity;
import org.example.service.TrainerService;
import org.example.service.UserCredentialsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TrainerServiceImpl implements TrainerService{
    private final UserRepository userRepository;
    private final TrainerRepository trainerRepository;
    private final UserCredentialsService userCredentialsService;
    private static final Logger logger = LoggerFactory.getLogger(TrainerServiceImpl.class);



    @Override
    public Long createTrainer(TrainerCreateDto trainerCreateDto) {
        String username = userCredentialsService.getUsername(
                trainerCreateDto.getFirstName(), trainerCreateDto.getLastName());

        String password = userCredentialsService.generatePassword();

        UserEntity user = userRepository.save(new UserEntity(
                null,
                trainerCreateDto.getLastName(),
                trainerCreateDto.getFirstName(),
                username,
                password,
                true
        ));

        TrainerEntity trainer = new TrainerEntity(
                null,
                trainerCreateDto.getSpecializationId(),
                user.getId()
        );

        TrainerEntity saved = trainerRepository.save(trainer);
        logger.info(String.format("New trainer created: %s", trainer.getId()));
        return saved.getId();
    }

    @Override
    public TrainerEntity updateTrainer(TrainerUpdateDto trainerUpdateDto, Long id) {
        TrainerEntity trainer = trainerRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Trainer is not found!")
        );

        if (trainerUpdateDto.getSpecializationId() != null){
            trainer.setSpecializationId(trainerUpdateDto.getSpecializationId());
        }

        logger.info(String.format("Trainer updated: %s", trainer.getId()));
        return trainerRepository.save(trainer);
    }

    @Override
    public TrainerResponseDto getTrainer(Long id) {
        TrainerEntity trainer = trainerRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Trainer is not found!")
        );

        TrainerResponseDto trainerResponseDto = new TrainerResponseDto();
        trainerResponseDto.setId(trainer.getId());
        trainerResponseDto.setSpecializationId(trainer.getSpecializationId());

        Optional<UserEntity> optionalUser = userRepository.findById(trainer.getUserId());
        if (optionalUser.isPresent()){
            UserEntity user = optionalUser.get();
            trainerResponseDto.setUsername(user.getUsername());
            trainerResponseDto.setLastName(user.getLastName());
            trainerResponseDto.setFirstName(user.getFirstName());
            trainerResponseDto.setPassword(user.getPassword());
            trainerResponseDto.setUserId(user.getId());
        }

        logger.info(String.format("Trainer read: %s", trainer.getId()));
        return trainerResponseDto;
    }
}
