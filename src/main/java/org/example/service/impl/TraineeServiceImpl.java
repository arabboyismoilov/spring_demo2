package org.example.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TraineeRepository;
import org.example.dao.UserRepository;
import org.example.dto.TraineeCreateDto;
import org.example.dto.TraineeResponseDto;
import org.example.dto.TraineeUpdateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.UserEntity;
import org.example.service.TraineeService;
import org.example.service.UserCredentialsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TraineeServiceImpl implements TraineeService {
    private final UserRepository userRepository;
    private final TraineeRepository traineeRepository;
    private final UserCredentialsService userCredentialsService;
    private static final Logger logger = LoggerFactory.getLogger(TraineeServiceImpl.class);

    @Override
    public Long createTrainee(TraineeCreateDto traineeCreateDto) {
        String username = userCredentialsService.getUsername(
                traineeCreateDto.getFirstName(), traineeCreateDto.getLastName());
        String password = userCredentialsService.generatePassword();

        UserEntity user = userRepository.save(new UserEntity(
                null,
                traineeCreateDto.getLastName(),
                traineeCreateDto.getFirstName(),
                username,
                password,
                true
        ));

        TraineeEntity trainee = new TraineeEntity(
                null,
                traineeCreateDto.getDateOfBirth(),
                traineeCreateDto.getAddress(),
                user.getId()
        );

        TraineeEntity saved = traineeRepository.save(trainee);
        logger.info(String.format("New trainee created: %s", saved.getId()));

        return saved.getId();
    }

    @Override
    public TraineeEntity updateTrainee(TraineeUpdateDto traineeUpdateDto, Long id) {
        TraineeEntity trainee = traineeRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Trainee not found!")
        );

        if (traineeUpdateDto.getAddress() != null){
            trainee.setAddress(traineeUpdateDto.getAddress());
        }

        if (traineeUpdateDto.getDateOfBirth() != null){
            trainee.setDateOfBirth(traineeUpdateDto.getDateOfBirth());
        }

        logger.info(String.format("Trainee updated: %s", trainee.getId()));
        return traineeRepository.save(trainee);
    }

    @Override
    public void deleteTrainee(Long id) {
        traineeRepository.delete(id);
        logger.info(String.format("Trainee deleted: %s", id));
    }

    @Override
    public TraineeResponseDto getTrainee(Long id) {
        TraineeEntity trainee = traineeRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Trainee not found!")
        );
        TraineeResponseDto traineeResponseDto = new TraineeResponseDto();
        traineeResponseDto.setId(trainee.getId());
        traineeResponseDto.setAddress(trainee.getAddress());
        traineeResponseDto.setDateOfBirth(trainee.getDateOfBirth());

        Optional<UserEntity> optionalUser = userRepository.findById(trainee.getUserId());
        if (optionalUser.isPresent()){
            UserEntity user = optionalUser.get();
            traineeResponseDto.setUsername(user.getUsername());
            traineeResponseDto.setLastName(user.getLastName());
            traineeResponseDto.setFirstName(user.getFirstName());
            traineeResponseDto.setPassword(user.getPassword());
            traineeResponseDto.setUserId(user.getId());
        }
        logger.info(String.format("Trainee read: %s", id));
        return traineeResponseDto;
    }
}
