package org.example.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.entity.*;
import org.example.service.ReadFileService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
public class ReadFileServiceImpl implements ReadFileService {
    private final ObjectMapper objectMapper;

    @Override
    public List<UserEntity> getUsers(String filePath) {
        try {
            File jsonFile = new File(filePath);
            return Arrays.asList(objectMapper.readValue(jsonFile, UserEntity[].class));
        } catch (IOException e) {
            System.out.println("Exception occurred");
            return new ArrayList<>();
        }
    }

    @Override
    public List<TrainingType> getTrainingType(String filePath) {
        try {
            File jsonFile = new File(filePath);
            return Arrays.asList(objectMapper.readValue(jsonFile, TrainingType[].class));
        } catch (IOException e) {
            System.out.println("Exception occurred");
            return new ArrayList<>();
        }
    }

    @Override
    public List<TrainingEntity> getTraining(String filePath) {
        try {
            File jsonFile = new File(filePath);
            return Arrays.asList(objectMapper.readValue(jsonFile, TrainingEntity[].class));
        } catch (IOException e) {
            System.out.println("Exception occurred");
            return new ArrayList<>();
        }
    }

    @Override
    public List<TraineeEntity> getTrainee(String filePath) {
        try {
            File jsonFile = new File(filePath);
            return Arrays.asList(objectMapper.readValue(jsonFile, TraineeEntity[].class));
        } catch (IOException e) {
            System.out.println("Exception occurred");
            return new ArrayList<>();
        }
    }

    @Override
    public List<TrainerEntity> getTrainer(String filePath) {
        try {
            File jsonFile = new File(filePath);
            return Arrays.asList(objectMapper.readValue(jsonFile, TrainerEntity[].class));
        } catch (IOException e) {
            System.out.println("Exception occurred");
            return new ArrayList<>();
        }
    }
}
