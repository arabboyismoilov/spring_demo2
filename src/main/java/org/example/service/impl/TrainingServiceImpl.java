package org.example.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TraineeRepository;
import org.example.dao.TrainerRepository;
import org.example.dao.TrainingRepository;
import org.example.dao.TrainingTypeRepository;
import org.example.dto.TrainingCreateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.TrainerEntity;
import org.example.entity.TrainingEntity;
import org.example.entity.TrainingType;
import org.example.service.TrainingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TrainingServiceImpl implements TrainingService {
    private final TrainingRepository trainingRepository;
    private final TrainerRepository trainerRepository;
    private final TraineeRepository traineeRepository;
    private final TrainingTypeRepository trainingTypeRepository;
    private static final Logger logger = LoggerFactory.getLogger(TrainingServiceImpl.class);



    @Override
    public Long createTraining(TrainingCreateDto trainingCreateDto) {
        TraineeEntity trainee = traineeRepository.findById(trainingCreateDto.getTraineeId()).orElseThrow(
                () -> new RuntimeException("Trainee not found!")
        );

        TrainerEntity trainer = trainerRepository.findById(trainingCreateDto.getTrainerId()).orElseThrow(
                () -> new RuntimeException("Trainer not found!")
        );

        TrainingType trainingType = trainingTypeRepository.findById(trainingCreateDto.getTrainingTypeId()).orElseThrow(
                () -> new RuntimeException("Training type not found!")
        );

        TrainingEntity training = new TrainingEntity(
                null,
                trainee.getId(),
                trainer.getId(),
                trainingCreateDto.getTrainingName(),
                trainingType.getId(),
                trainingCreateDto.getTrainingDate(),
                trainingCreateDto.getDuration()
        );
        TrainingEntity saved = trainingRepository.save(training);
        logger.info(String.format("New training created: %s", training.getId()));
        return saved.getId();
    }

    @Override
    public TrainingEntity getTraining(Long id) {
        logger.info(String.format("Training read: %s", id));
        return trainingRepository.findById(id).orElseThrow(
                () -> new RuntimeException("Training not found!")
        );
    }
}
