package org.example.service.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.UserRepository;
import org.example.service.UserCredentialsService;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserCredentialsServiceImpl implements UserCredentialsService {
    private final UserRepository userRepository;
    @Override
    public String getUsername(String firstName, String lastName) {
        String username = null;
        int serialNumber = 0;
        while (username == null){
            String str = firstName.toLowerCase() + "." + lastName.toLowerCase();
            if (serialNumber!=0){
                str+=serialNumber;
            }
            boolean isExist = userRepository.existByUsername(str);
            if (!isExist){
                username = str;
            }
            serialNumber++;
        }
        return username;
    }

    @Override
    public String generatePassword() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString().substring(0,10);
    }
}
