package org.example.service;

import org.example.dto.TrainingCreateDto;
import org.example.entity.TrainerEntity;
import org.example.entity.TrainingEntity;

public interface TrainingService {
    Long createTraining(TrainingCreateDto trainingCreateDto);
    TrainingEntity getTraining(Long id);
}
