package org.example.service;

import org.example.dto.TraineeCreateDto;
import org.example.dto.TrainerCreateDto;
import org.example.dto.TrainerResponseDto;
import org.example.dto.TrainerUpdateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.TrainerEntity;

public interface TrainerService {
    Long createTrainer(TrainerCreateDto trainerCreateDto);
    TrainerEntity updateTrainer(TrainerUpdateDto trainerUpdateDto, Long id);
    TrainerResponseDto getTrainer(Long id);
}
