package org.example.service;

public interface UserCredentialsService {
    String getUsername(String firstName, String lastName);
    String generatePassword();
}
