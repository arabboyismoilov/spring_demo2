package org.example.service;

import org.example.entity.*;

import java.util.List;

public interface ReadFileService {
    List<UserEntity> getUsers(String filePath);
    List<TrainingType> getTrainingType(String filePath);
    List<TrainingEntity> getTraining(String filePath);
    List<TraineeEntity> getTrainee(String filePath);
    List<TrainerEntity> getTrainer(String filePath);
}
