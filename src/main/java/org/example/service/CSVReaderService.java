package org.example.service;

import org.example.entity.*;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CSVReaderService {
    List<UserEntity> readUsers(String filePath);
    List<TrainingType> readTrainingType(String filePath);
    List<TrainingEntity> readTrainings(String filePath);
    List<TraineeEntity> readTrainee(String filePath);
    List<TrainerEntity> readTrainers(String filePath);
}
