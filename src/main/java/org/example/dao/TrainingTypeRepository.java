package org.example.dao;

import org.example.entity.TrainingType;

import java.util.Optional;

public interface TrainingTypeRepository {
    Optional<TrainingType> findById(Long id);
}
