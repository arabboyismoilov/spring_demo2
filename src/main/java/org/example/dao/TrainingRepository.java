package org.example.dao;

import org.example.entity.TrainingEntity;

import java.util.Optional;

public interface TrainingRepository {
    TrainingEntity save(TrainingEntity training);
    Optional<TrainingEntity> findById(Long id);
    void delete(Long id);
}
