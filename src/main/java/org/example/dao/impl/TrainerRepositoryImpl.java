package org.example.dao.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TrainerRepository;
import org.example.dao.database.Storage;
import org.example.dto.TraineeCreateDto;
import org.example.entity.TraineeEntity;
import org.example.entity.TrainerEntity;
import org.example.service.TraineeService;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TrainerRepositoryImpl implements TrainerRepository {
    private final Storage storage;

    @Override
    public TrainerEntity save(TrainerEntity trainer) {
        if(trainer.getId() == null){
            trainer.setId(System.currentTimeMillis());
        }
        HashMap<Long, TrainerEntity> trainerData = storage.getTrainerData();
        trainerData.put(trainer.getId(), trainer);
        return trainer;
    }

    @Override
    public Optional<TrainerEntity> findById(Long id) {
        HashMap<Long, TrainerEntity> trainerData = storage.getTrainerData();
        if (!trainerData.containsKey(id)){
            return Optional.empty();
        }
        return Optional.of(trainerData.get(id));
    }

    @Override
    public void delete(Long id) {
        HashMap<Long, TrainerEntity> trainerData = storage.getTrainerData();
        trainerData.remove(id);
    }
}
