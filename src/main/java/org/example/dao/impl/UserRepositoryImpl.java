package org.example.dao.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.UserRepository;
import org.example.dao.database.Storage;
import org.example.entity.UserEntity;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {
    private final Storage storage;

    @Override
    public Optional<UserEntity> findById(Long id) {
        HashMap<Long, UserEntity> userData = storage.getUsersData();
        if (!userData.containsKey(id)){
            return Optional.empty();
        }
        return Optional.of(userData.get(id));
    }

    @Override
    public boolean existByUsername(String username) {
        HashMap<Long, UserEntity> userData = storage.getUsersData();
        return userData.values().stream()
                .anyMatch(user -> user.getUsername().equals(username));
    }

    @Override
    public UserEntity save(UserEntity user) {
        if (user.getId() == null){
            user.setId(System.currentTimeMillis());
        }
        HashMap<Long, UserEntity> userData = storage.getUsersData();
        userData.put(user.getId(), user);
        return user;
    }

    @Override
    public void delete(Long id) {
        HashMap<Long, UserEntity> userData = storage.getUsersData();
        userData.remove(id);
    }
}
