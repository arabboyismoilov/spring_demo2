package org.example.dao.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TrainingTypeRepository;
import org.example.dao.database.Storage;
import org.example.entity.TrainingType;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TrainingTypeRepositoryImpl implements TrainingTypeRepository {
    private final Storage storage;

    @Override
    public Optional<TrainingType> findById(Long id) {
        HashMap<Long, TrainingType> trainingTypeData = storage.getTrainingTypeData();
        if (!trainingTypeData.containsKey(id)){
            return Optional.empty();
        }
        return Optional.of(trainingTypeData.get(id));
    }
}
