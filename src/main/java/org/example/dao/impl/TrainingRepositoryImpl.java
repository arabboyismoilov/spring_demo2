package org.example.dao.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TrainingRepository;
import org.example.dao.database.Storage;
import org.example.entity.TrainingEntity;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class TrainingRepositoryImpl implements TrainingRepository {
    private final Storage storage;

    @Override
    public TrainingEntity save(TrainingEntity training) {
        if (training.getId() == null){
            training.setId(System.currentTimeMillis());
        }
        HashMap<Long, TrainingEntity> trainingData = storage.getTrainingData();
        trainingData.put(training.getId(), training);
        return training;
    }

    @Override
    public Optional<TrainingEntity> findById(Long id) {
        HashMap<Long, TrainingEntity> trainingData = storage.getTrainingData();
        if(!trainingData.containsKey(id)){
            return Optional.empty();
        }
        return Optional.of(trainingData.get(id));
    }

    @Override
    public void delete(Long id) {
        HashMap<Long, TrainingEntity> trainingData = storage.getTrainingData();
        trainingData.remove(id);
    }
}
