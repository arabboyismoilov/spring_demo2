package org.example.dao.impl;

import lombok.RequiredArgsConstructor;
import org.example.dao.TraineeRepository;
import org.example.dao.database.Storage;
import org.example.entity.TraineeEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TraineeRepositoryImpl implements TraineeRepository {
    private final Storage storage;

    @Override
    public TraineeEntity save(TraineeEntity trainee) {
        if (trainee.getId()==null){
            trainee.setId(System.currentTimeMillis());
        }
        HashMap<Long, TraineeEntity> traineeData = storage.getTraineeData();
        traineeData.put(trainee.getId(), trainee);
        return trainee;
    }

    @Override
    public Optional<TraineeEntity> findById(Long id) {
        HashMap<Long, TraineeEntity> traineeData = storage.getTraineeData();
        if (traineeData.get(id)==null){
            return Optional.empty();
        }
        return Optional.of(traineeData.get(id));
    }

    @Override
    public void delete(Long id) {
        HashMap<Long, TraineeEntity> traineeData = storage.getTraineeData();
        traineeData.remove(id);
    }
}
