package org.example.dao;

import org.example.entity.TraineeEntity;

import java.util.Optional;

public interface TraineeRepository {
    TraineeEntity save(TraineeEntity trainee);
    Optional<TraineeEntity> findById(Long id);
    void delete(Long id);
}
