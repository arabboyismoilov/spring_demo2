package org.example.dao;

import org.example.entity.TraineeEntity;
import org.example.entity.TrainerEntity;

import java.util.Optional;

public interface TrainerRepository {
    TrainerEntity save(TrainerEntity trainer);
    Optional<TrainerEntity> findById(Long id);
    void delete(Long id);
}
