package org.example.dao;

import org.example.entity.UserEntity;

import java.util.Optional;

public interface UserRepository {
    Optional<UserEntity> findById(Long id);
    boolean existByUsername(String username);

    UserEntity save(UserEntity user);
    void delete(Long id);
}
