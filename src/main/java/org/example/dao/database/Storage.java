package org.example.dao.database;

import lombok.Getter;
import org.example.entity.*;
import org.example.service.ReadFileService;

import java.util.HashMap;
import java.util.List;

@Getter
public class Storage {
    private final ReadFileService readFileService;
    private final String data;

    private final HashMap<Long, UserEntity> usersData = new HashMap<>(); // key: id of object
    private final HashMap<Long, TrainingType> trainingTypeData = new HashMap<>(); // key: id of object
    private final HashMap<Long, TraineeEntity>  traineeData = new HashMap<>(); // key: id of object
    private final HashMap<Long, TrainingEntity> trainingData = new HashMap<>(); // key: id of object
    private final HashMap<Long, TrainerEntity> trainerData = new HashMap<>(); // key: id of object

    public Storage(ReadFileService readFileService, String data) {
        this.readFileService = readFileService;
        this.data = data;
    }


    public void init(){
        //initiate default users
        List<UserEntity> users = readFileService.getUsers(data+"/users.json");
        for (UserEntity user: users){
            usersData.put(user.getId(), user);
        }

        //initiate default training types
        List<TrainingType> types = readFileService.getTrainingType(data+"/trainingTypes.json");
        for (TrainingType trainingType: types){
            trainingTypeData.put(trainingType.getId(), trainingType);
        }

        List<TraineeEntity> traineeEntities = readFileService.getTrainee(data+"/trainees.json");
        for (TraineeEntity trainee: traineeEntities){
            traineeData.put(trainee.getId(), trainee);
        }

        List<TrainerEntity> trainers = readFileService.getTrainer(data+"/trainers.json");
        for (TrainerEntity trainer: trainers){
            trainerData.put(trainer.getId(), trainer);
        }

        List<TrainingEntity> trainings = readFileService.getTraining(data+"/trainings.json");
        for (TrainingEntity training: trainings){
            trainingData.put(training.getId(), training);
        }
    }
}
