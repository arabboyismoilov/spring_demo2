package org.example.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TrainerEntity{
    private Long id;
    private Long specializationId; //training type id
    private Long userId;

    public TrainerEntity(Long id){
        this.id = id;
    }
}
