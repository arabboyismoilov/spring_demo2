package org.example.entity;

import lombok.*;

import java.time.LocalDate;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class TraineeEntity {
    private Long id;
    private String dateOfBirth;
    private String address;
    private Long userId;

    public TraineeEntity(Long id){
        this.id = id;
    }
}
