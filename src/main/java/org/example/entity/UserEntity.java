package org.example.entity;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class UserEntity {
    private Long id;
    private String lastName;
    private String firstName;
    private String username;
    private String password;
    private boolean active;

    public UserEntity(Long id){
        this.id = id;
    }
}
